package scrum.stateimpl;

import scrum.State;
import scrum.Task;

public class SprintState implements State {
    @Override
    public void goToInProgress(Task task) {
        task.setState(new InProgressState());
        System.out.println("Now task is IN PROGRESS");
    }

    @Override
    public void goToBlocked(Task task) {
        task.setState(new BlockedState());
        System.out.println("Oops it's BLOCKED");
    }
}
