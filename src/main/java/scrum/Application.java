package scrum;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        Task task = null;
        do {
            System.out.println("   1 - Create new task" + "         2 - task -> SPRINT   ");
            System.out.println("   3 - task -> IN PROGRESS  " + "   4 - task ->PEER REVIEW   ");
            System.out.println("   5 - task -> IN TEST      " + "   6 - task -> DONE     ");
            System.out.println("   7 - task -> BLOCKED" + "         8 - show task state ");
            System.out.println("   Q - exit");
            System.out.println("   Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        task = new Task();
                        System.out.println("New task created and you are working  with it in this moment");
                        break;
                    case "2":
                        task.goToSprint();
                        break;
                    case "3":
                        task.goToInProgress();
                        break;
                    case "4":
                        task.goToPeerReview();
                        break;
                    case "5":
                       task.goToInTest();
                        break;
                    case "6":
                        task.goToDone();
                        break;
                    case "7":
                        task.goToBlocked();
                        break;
                    case "8":
                        if (task!=null) {
                            System.out.println("It has "+task.getState().getClass().getName().substring(16));
                        }else {
                            System.out.println("Create new Task, before to see it's state");
                        }
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
