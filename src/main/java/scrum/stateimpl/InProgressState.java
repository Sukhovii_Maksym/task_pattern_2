package scrum.stateimpl;

import scrum.State;
import scrum.Task;

public class InProgressState implements State {
    @Override
    public void goToPeerReview(Task task) {
        task.setState(new PeerReviewState());
        System.out.println("Now task is in PEER REVIEW");
    }

    @Override
    public void goToBlocked(Task task) {
        task.setState(new BlockedState());
        System.out.println("Oops it's BLOCKED");
    }
}
