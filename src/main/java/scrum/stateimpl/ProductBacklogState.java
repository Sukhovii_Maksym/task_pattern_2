package scrum.stateimpl;
import scrum.State;
import scrum.Task;

public class ProductBacklogState implements State {
    @Override
    public void goToSprint(Task task) {
        task.setState(new SprintState());
        System.out.println("Now task is in SPRINT");
    }

    @Override
    public void goToBlocked(Task task) {
        task.setState(new BlockedState());
        System.out.println("Oops it's BLOCKED");
    }
}
