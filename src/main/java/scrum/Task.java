package scrum;

import scrum.stateimpl.ProductBacklogState;

public class Task {
    private State state;

    public Task(){
        state= new ProductBacklogState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
    public void goToSprint(){state.goToSprint(this);}
    public void goToInProgress(){state.goToInProgress(this);}
    public void goToPeerReview(){state.goToPeerReview(this);}
    public void goToInTest(){ state.goToInTest(this);    }
    public void goToDone(){
        state.goToDone(this);
    }
    public void goToBlocked(){state.goToBlocked(this);}

}
