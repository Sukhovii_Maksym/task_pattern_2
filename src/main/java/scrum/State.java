package scrum;

public interface State {
    default void goToSprint(Task task){
        System.out.println("go to SPRINT is not allowed for this task");
    }
    default void goToInProgress(Task task){
        System.out.println("go to IN PROGRESS is not allowed for this task");
    }
    default void goToPeerReview(Task task){
        System.out.println("go to PEER REVIEW is not allowed for this task");
    }
    default void goToInTest(Task task){
        System.out.println("go to IN TEST is not allowed for this task");
    }
    default void goToDone(Task task){
        System.out.println("go to DONE is not allowed for this task");
    }
    default void goToBlocked(Task task){
        System.out.println("go to BLOCKED is not allowed for this task");
    }
}
